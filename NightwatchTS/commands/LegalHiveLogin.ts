import { NightwatchBrowser } from "nightwatch";
import drier from "../commands/drier/index"
const {
    get,
    await,
    inputText,
    click,
    inputTextSimple,
    screenshot
} = drier


const nouns = {

    urls: 'https://flexmanagedev1.sharepoint.com/sites/LHQA/SitePages/Index.aspx',
    username: {
        text: "admin@flexmanagedev1.onmicrosoft.com",
        selector: "i0116",
        click: "idSIButton9"
    },
    password: {
        text: "LegalOxDesk1!",
        selector: "i0118",
        click: "idSIButton9"
    },
    
    forgetAuthenticationButton: "#idBtn_Back"
    
};



exports.command = function () : NightwatchBrowser{
        const browser : NightwatchBrowser = this

        browser
        .utils(get, {url: nouns.urls})
        .utils(inputTextSimple, nouns.username)
        .utils(screenshot, {name: "login"})
        .utils(click,{ selector: `input[id=${nouns.username.click}]`  })
        .utils(screenshot, {name: "click"})
        .utils(inputTextSimple, nouns.password)
        .utils(click, {selector: `input[id=${nouns.password.click}]` })
        .utils(click, {selector: nouns.forgetAuthenticationButton })
        
        

        return this

}