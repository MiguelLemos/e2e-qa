import { NightwatchBrowser, NightwatchCallbackResult } from "nightwatch";
import drier from "../commands/drier/index"
const {
    get,
    await,
    inputText,
    click,
    inputTextSimple,
    screenshot,
    iframe
} = drier


const nouns = {

    
    matterPageNavigation:{
        click: "div.col-md-2:nth-child(4) > div:nth-child(1)"  
    }, 
    matterFormOpen:{
        click: "div.details-column:nth-child(2)" 
    },
    formFill:{
        title:{
            selector: "Title",
            text: "This title is a test Complete"
        },
        clientCode:{
            selector: "MatterCode",
            text: "This matterCode is a test ABC123. ABC123 for the win 1.0."
        },
        click:"input[id^='ctl00_ctl39_g_6b8e5c2b_19d0_42f6_8aaf_69556b99ecb4_ctl00_toolBarTbl_RightRptControls_ctl00_ctl00_diidIOSaveItem']"
        
    },

};



exports.command = function () : NightwatchBrowser{

    
    click(this, {
        selector: nouns.matterPageNavigation.click
    })
    click(this, {
        selector: nouns.matterFormOpen.click
    })
    

    const decorated = () => {
        inputTextSimple(this, nouns.formFill.title)     
        inputTextSimple(this, nouns.formFill.clientCode)  
        click(this, {selector: nouns.formFill.click})
    }

    const iframe_params =  {
        beggining_of_iframe_id:"DlgFrame",
        decorated
    }
    iframe(this, iframe_params)

    return this

}