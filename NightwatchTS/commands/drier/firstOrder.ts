import base = require("./base");
import { NightwatchBrowser } from "nightwatch";

export const utils =  {
    click: (browser, {selector}) => 
        base.utils.await(browser, {selector})
        .click(selector),
    inputText: (browser: NightwatchBrowser, {selector, text}) => 
        base.utils.await(browser, {selector})
        .setValue(selector, text),
}
