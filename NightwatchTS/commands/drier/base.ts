import { NightwatchBrowser } from "nightwatch";

export const  utils =  {
    get: (browser: NightwatchBrowser, {url}) : NightwatchBrowser => 
        browser.url(url),
    await: (browser: NightwatchBrowser, {selector}) : NightwatchBrowser=> 
        browser.waitForElementVisible(selector, 60000),
    screenshot: (browser: NightwatchBrowser, {name}): NightwatchBrowser => 
        browser.saveScreenshot(`./screenshots/ ${name}.png`)

}
