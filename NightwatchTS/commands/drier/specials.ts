import { NightwatchBrowser } from "nightwatch";

export const  utils =  {
    iframe: (browser: NightwatchBrowser, 
            {
                beggining_of_iframe_id,
                decorated
            }) => {            
                const default_iframe = "DlgFrame"
                const iframe_selector = `iframe[id^='${beggining_of_iframe_id || default_iframe}']`


                const insideFrame = (iframe_selector, foo) => 
                        browser.element("css selector", iframe_selector, iframe =>               
                        browser
                        .frame(iframe.value, () => decorated())
                        .frameParent()
                    )
                insideFrame(iframe_selector, decorated)           
        }

}
