import firstOrder = require("./firstOrder");
import { NightwatchBrowser } from "nightwatch";

export const utils =  {
    inputTextSimple: (browser: NightwatchBrowser, item) => 
        firstOrder.utils.inputText(
            browser, 
            {
                  selector: `input[id^=${item.selector}]`
                , text: item.text
            })
}

