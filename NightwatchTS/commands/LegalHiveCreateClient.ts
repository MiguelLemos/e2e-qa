import { NightwatchBrowser, NightwatchCallbackResult } from "nightwatch";
import drier from "../commands/drier/index"
const {
    get,
    await,
    inputText,
    click,
    inputTextSimple,
    screenshot,
    iframe
} = drier


const nouns = {

    
    clientPageNavigation:{
        click: "div.col-md-2:nth-child(3) > div:nth-child(1)"  
    }, 
    clientFormOpen:{
        click: "div.details-column:nth-child(2)" 
    },
    formFill:{
        title:{
            selector: "Title",
            text: "This title is a test Complete"
        },
        clientCode:{
            selector: "ClientCode",
            text: "This clientCode is a test. ABC123. ABC123 for the win 1.0.0."
        },
        click:"input[id^='ctl00_ctl39_g_cd8216b9_c53b_4f82_9720_6fa869f25d89_ctl00_toolBarTbl_RightRptControls_ctl00_ctl00_diidIOSaveItem']"
    },

};





exports.command = function () : NightwatchBrowser{

    
    const browser : NightwatchBrowser = this

    const decorated = () => {
        inputTextSimple(browser, nouns.formFill.title)     
        inputTextSimple(browser, nouns.formFill.clientCode)  
        click(browser, {selector: nouns.formFill.click})
    }
    
    const iframe_params =  {
        beggining_of_iframe_id:"DlgFrame",
        decorated
    }

    
    browser
        .utils(click, {selector: nouns.clientPageNavigation.click})
        .utils(click, {selector: nouns.clientFormOpen.click})
        .utils(iframe, iframe_params)

    return this

}