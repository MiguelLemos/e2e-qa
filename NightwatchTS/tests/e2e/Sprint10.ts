import { NightwatchBrowser } from 'nightwatch';

export = {

    'Demo test Google': function (client: NightwatchBrowser) {
        client
            .LegalHiveLogin()
            .LegalHiveCreateClient()
            .LegalHiveCreateMatter()
            client.pause()
    }
    
};