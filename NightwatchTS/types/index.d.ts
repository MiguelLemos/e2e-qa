import * as NW from "nightwatch";

// merge interfaces with nightwatch types
declare module "nightwatch" {
    export interface NightwatchCustomAssertions {
        screenshotEquals(this: NW.NightwatchBrowser, filename: string, tolerance?: number, callback?: Function): NW.NightwatchBrowser;
    }

    export interface NightwatchCustomCommands {
        LegalHiveLogin(this: NW.NightwatchBrowser): NW.NightwatchBrowser;

        LegalHiveCreateClient(this: NW.NightwatchBrowser): NW.NightwatchBrowser;

        LegalHiveCreateMatter(this: NW.NightwatchBrowser): NW.NightwatchBrowser;

        utils(this: NW.NightwatchBrowser, foo: Function, fooParams: Object): NW.NightwatchBrowser

        compareScreenshot(this: NW.NightwatchBrowser, filename: string, tolerance?: number, callback?: Function): NW.NightwatchBrowser
    }
  
}